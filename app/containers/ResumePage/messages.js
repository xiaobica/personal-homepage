/*
 * ResumePage Messages
 *
 * This contains all the text for the ResumePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  name: {
    id: 'boilerplate.containers.ResumePage.name',
    defaultMessage: 'Eric Wang',
  },
  education: {
    id: 'boilerplate.containers.ResumePage.education',
    defaultMessage: 'Education',
  },
  massey: {
    id: 'boilerplate.containers.ResumePage.massey',
    defaultMessage: 'Massey University (02/2016-12/2016)',
  },
  masseyDescription: {
    id: 'boilerplate.containers.ResumePage.masseyDescription',
    defaultMessage: 'Graduate Diploma in Information Science, Average A',
  },
  centralSouth: {
    id: 'boilerplate.containers.ResumePage.centralSouth',
    defaultMessage: 'Central South University (09/2008-07/2012)',
  },
  centralSouthDescription: {
    id: 'boilerplate.containers.ResumePage.centralSouthDescription',
    defaultMessage: 'Bachelor Diploma in Software Engineering',
  },
  technique: {
    id: 'boilerplate.containers.ResumePage.technique',
    defaultMessage: 'TECHNICAL EXPERTISE',
  },
  frontend: {
    id: 'boilerplate.containers.ResumePage.technique.frontend',
    defaultMessage: 'FRONT-END TECHNOLOGIES',
  },
  frontendDescription: {
    id: 'boilerplate.containers.ResumePage.technique.frontendDescription',
    defaultMessage: 'Angular, React, MVVM, FLUX, HTML, JavaScript, Typescript, CSS, Bootstrap, Semantic-UI, JQuery',
  },
  backend: {
    id: 'boilerplate.containers.ResumePage.technique.backend',
    defaultMessage: 'BACK-END TECHNOLOGIES',
  },
  backendDescription: {
    id: 'boilerplate.containers.ResumePage.technique.backendDescription',
    defaultMessage: 'C#, ASP.NET MVC, RESTful Api, Entity Framework, PHP Laravel',
  },
  database: {
    id: 'boilerplate.containers.ResumePage.technique.database',
    defaultMessage: 'DATABASE',
  },
  databaseDescription: {
    id: 'boilerplate.containers.ResumePage.technique.databaseDescription',
    defaultMessage: 'Microsoft SQL Server, MySQL, Oracle, SQL query, Stored Procedure, Functions, Triggers',
  },
  tool: {
    id: 'boilerplate.containers.ResumePage.technique.tool',
    defaultMessage: 'TOOLS/LIBRARIES',
  },
  toolDescription: {
    id: 'boilerplate.containers.ResumePage.technique.toolDescription',
    defaultMessage: 'Visual Studio, Visual Studio Code, Git, Node.js, Composer, WebPack, Auto Mapper',
  },
  experience: {
    id: 'boilerplate.containers.ResumePage.experience',
    defaultMessage: 'PROFESSIONAL EXPERIENCE',
  },
  centralStation: {
    id: 'boilerplate.containers.ResumePage.experience.centralStation',
    defaultMessage: 'Central Station Software (Full-Stack Developer, 10/2017 – Present)',
  },
  centralStationDescription: {
    id: 'boilerplate.containers.ResumePage.experience.centralStationDescription',
    defaultMessage: 'Central Station Software provides cloud-based web/BI solutions in latest technologies. I worked as a full-stack developer in the early childhood education team, providing solutions for kindergarten management.',
  },
  centralStationExperience1: {
    id: 'boilerplate.containers.ResumePage.experience.centralStationExperience1',
    defaultMessage: 'Kendo UI, JQuery, Bootstrap, Semantic UI and handlebars on client-side development',
  },
  centralStationExperience2: {
    id: 'boilerplate.containers.ResumePage.experience.centralStationExperience2',
    defaultMessage: '.net core, entity framework on server-side development',
  },
  centralStationExperience3: {
    id: 'boilerplate.containers.ResumePage.experience.centralStationExperience3',
    defaultMessage: 'TFS, bower and multi-layer development',
  },
  guoChuang: {
    id: 'boilerplate.containers.ResumePage.experience.guoChuang',
    defaultMessage: 'Guo Chuang Software Co., Ltd.',
  },
  guoChuangDescription: {
    id: 'boilerplate.containers.ResumePage.experience.guoChuangDescription',
    defaultMessage: 'Guo Chuang Software CO., Ltd is a cloud-based software solution provider. I worked as a database developer at the Department of the power industry, designing and developing for our data processing systems, data integration systems, and data warehouse.',
  },
  guoChuangExperience1: {
    id: 'boilerplate.containers.ResumePage.experience.guoChuangExperience1',
    defaultMessage: 'Database Design and Implementation using Power Designer',
  },
  guoChuangExperience2: {
    id: 'boilerplate.containers.ResumePage.experience.guoChuangExperience2',
    defaultMessage: 'Oracle PL/SQL Development (Stored procedures, views and triggers)',
  },
  guoChuangExperience3: {
    id: 'boilerplate.containers.ResumePage.experience.guoChuangExperience3',
    defaultMessage: 'Report Development on fine report platform',
  },
  openSource: {
    id: 'boilerplate.containers.ResumePage.openSource',
    defaultMessage: 'GITHUB/NPM CONTRIBUTIONS',
  },
  homepage: {
    id: 'boilerplate.containers.ResumePage.homepage',
    defaultMessage: 'My Personal Homepage',
  },
  openSourceDescription1: {
    id: 'boilerplate.containers.ResumePage.openSourceDescription1',
    defaultMessage: 'A code generator based on node environment. It can be used to generate code of Angular-Ngrx automatically.',
  },
  openSourceDescription2: {
    id: 'boilerplate.containers.ResumePage.openSourceDescription2',
    defaultMessage: 'An app about baking recipes. Server side is implemented in PHP Laravel, website in Angular2, mobile in Ionic2. State management code is shared between the website and mobile.',
  },
  openSourceDescription3: {
    id: 'boilerplate.containers.ResumePage.openSourceDescription3',
    defaultMessage: 'This is my personal homepage, written in React, Redux and Semantic-UI.',
  },
  referees: {
    id: 'boilerplate.containers.ResumePage.referees',
    defaultMessage: 'REFEREES',
  },
  refereesContent: {
    id: 'boilerplate.containers.ResumePage.refereesContent',
    defaultMessage: 'Available on request',
  },
});
